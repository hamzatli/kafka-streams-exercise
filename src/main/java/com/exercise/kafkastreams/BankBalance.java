package com.exercise.kafkastreams;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.Deserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.Serializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KTable;
import org.apache.kafka.streams.kstream.Produced;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.time.Instant;
import java.util.List;
import java.util.Properties;

public class BankBalance {

    private static final String INPUT_TOPIC_NAME = "kafka-favorite-color-input";
    private static final String OUTPUT_TOPIC_NAME = "kafka-favorite-color-output";

    public static void main(String[] args) {

        Properties config = new Properties();
        config.put(StreamsConfig.APPLICATION_ID_CONFIG, "favorite-color-application");
        config.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        config.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        config.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        //Do not use in production
        config.put(StreamsConfig.CACHE_MAX_BYTES_BUFFERING_CONFIG, "0");

        // json Serde
        final Serializer<JsonNode> jsonSerializer = new JsonSerializer();
        final Deserializer<JsonNode> jsonDeserializer = new JsonDeserializer();
        final Serde<JsonNode> jsonSerde = Serdes.serdeFrom(jsonSerializer, jsonDeserializer);

        // create the initial json object for balances
        ObjectNode initialBalance = JsonNodeFactory.instance.objectNode();
        initialBalance.put("count", 0);
        initialBalance.put("balance", 0);
        initialBalance.put("time", Instant.ofEpochMilli(0L).toString());


        StreamsBuilder builder = new StreamsBuilder();

        KStream<String, JsonNode> bankTransactions = builder.stream(INPUT_TOPIC_NAME);

        KTable<String, JsonNode> bankBalance = bankTransactions
                .
//
//        favoriteColors.toStream()
//                .to(OUTPUT_TOPIC_NAME, Produced.with(Serdes.String(), Serdes.Long()));
//
//        KafkaStreams streams = new KafkaStreams(builder.build(), config);
//        streams.start();
//        // shutdown hook to correctly close the streams application
//        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
//        System.out.println("Hello word");
//    }
//
//    private static JsonNode newBalance(JsonNode transaction, JsonNode balance) {
//        // create a new balance json object
//        ObjectNode newBalance = JsonNodeFactory.instance.objectNode();
//        newBalance.put("count", balance.get("count").asInt() + 1);
//        newBalance.put("balance", balance.get("balance").asInt() + transaction.get("amount").asInt());
//
//        Long balanceEpoch = Instant.parse(balance.get("time").asText()).toEpochMilli();
//        Long transactionEpoch = Instant.parse(transaction.get("time").asText()).toEpochMilli();
//        Instant newBalanceInstant = Instant.ofEpochMilli(Math.max(balanceEpoch, transactionEpoch));
//        newBalance.put("time", newBalanceInstant.toString());
//        return newBalance;
//    }
    }
}
