docker pull johnnypark/kafka-zookeeper

docker run -d --rm -p 2181:2181 -p 9092:9092 -e ADVERTISED_HOST=127.0.0.1  -e NUM_PARTITIONS=1 johnnypark/kafka-zookeeper

## Log compaction kafka topic setting
kafka-topics --zookeeper 127.0.0.1:2181 --create
--topic <some_topic_name> --partitions 1 --replica-factor 1 
--config cleanup.policy=compact
--config min.cleanup.dirty.ratio=0.5
--config segment.ms=10000

kafka-topics --zookeeper 127.0.0.1:2181 --create --topic kafka-log-compaction --partitions 1 --replication-factor 1 --config min.cleanable.dirty.ratio=0.005 --config segment.ms=10000